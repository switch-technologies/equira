module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Equira 🚀',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Espacio de empredimiento' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
 loading: { color: '#3139D4' },
 css: [
   '~/assets/main.css'
 ],
 modules: [
    'nuxt-buefy',
    '@nuxtjs/axios'
  ],
  axios: {
    baseURL: 'http://localhost:3333'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */

    extend (config, { isDev, isClient }) {
      config.module.rules.push(
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url'
        }
      )
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

